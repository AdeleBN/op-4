all: ex41.out ex42.out

ex41.out: ex41.o
	gcc -Werror -g -o ex41.out ex41.o -pthread

ex42.out: ex42.o
	gcc -Werror -g -o ex42.out ex42.o -pthread

ex41.o: ex41.c
	gcc -Werror -g -c -o ex41.o ex41.c

ex42.o: ex42.c
	gcc -Werror -g -c -o ex42.o ex42.c

clean:
	rm -f *.o ex42.out ex41.out
