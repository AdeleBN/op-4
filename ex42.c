/****************************************
* Student name: Adele Bendayan
* Student ID: 336141056
* Course Exercise Group: 05
* Exercise name: Exercise 04 part 2
*****************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/stat.h>
#include <sys/shm.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/wait.h>
#include <stdbool.h>
#include <signal.h>
#include <string.h>
#include <ctype.h>
#include <pthread.h>
#include <sys/prctl.h>
#include <semaphore.h>
#include <time.h>

#define SHM_SIZE 1024

sem_t * sem_read;
sem_t * sem_write;
int threads_on_hold;
int threads_keepalive;
int internal_count;
int fd;

/**
 * Structure for a job
 */
typedef struct job{
	struct job*  prev; // pointer to previous job
	void (*function)(void* arg); // function pointer
	void* arg; // function's argument
} job;

/**
 * Structure for the job queue
 */
typedef struct jobqueue{
	pthread_mutex_t rwmutex; // used for queue r/w access
	job *front; // pointer to front of queue
	job *rear; // pointer to rear  of queue
	sem_t *has_jobs; // flag as binary semaphore
	int len; // number of jobs in queue
} jobqueue;

/**
 * Structure for a thred
 */
typedef struct thread{
	int id; // friendly id
	pthread_t pthread; // pointer to actual thread
	struct thpool_* thpool_p; // access to thpool
} thread;

/**
 * Structure for the threadpool
 */
typedef struct thpool_{
	thread** threads; // pointer to threads
	volatile int num_threads_alive; // threads currently alive
	volatile int num_threads_working; // threads currently working
	pthread_mutex_t thcount_lock; // used for thread count etc
	pthread_cond_t threads_all_idle; // signal to ThpoolWait
	jobqueue jobqueue;// job queue
} thpool_;

/**
 * The prototypes
 */

// for the thread
static int ThreadInit(thpool_* thpool_p, struct thread** thread_p, int id);
static void* ThreadDo(struct thread* thread_p);

// for the jobqueue
static int JobqueueInit(jobqueue* jobqueue_p);
static void JobqueueClear(jobqueue* jobqueue_p);
static void JobqueuePush(jobqueue* jobqueue_p, struct job* newjob_p);
static struct job* JobqueuePull(jobqueue* jobqueue_p);
static void JobqueueDestroy(jobqueue* jobqueue_p);

// for the threadpool
struct thpool_* ThpoolInit(int num_threads);
int ThpoolAddWork(thpool_* thpool_p, void (*function_p)(void*), void* arg_p);
void ThpoolWait(thpool_* thpool_p);
void ThpoolPause(thpool_* thpool_p);

// other functions
void SleepAndAdd(void* number_to_add);
void ThreadIdentifier();
void PrintString(char* stringToPrint);

/************************************************************************************************
 * function name: ThpoolInit                                                                *
* The input: int                                                                   *
* The output: thpool_*                                                                                *
* The function operation: Initialise thread pool                        *
*************************************************************************************************/
struct thpool_* ThpoolInit(int num_threads) {

	threads_on_hold   = 0;
	threads_keepalive = 1;

	if (num_threads < 0){
		num_threads = 0;
	}

	/* Make new thread pool */
	thpool_* thpool_p;
	thpool_p = (struct thpool_*)malloc(sizeof(struct thpool_));
	if (thpool_p == NULL){
		perror("ThpoolInit(): Could not allocate memory for thread pool\n");
		return NULL;
	}
	thpool_p->num_threads_alive   = 0;
	thpool_p->num_threads_working = 0;

	/* Initialise the job queue */
	if (JobqueueInit(&thpool_p->jobqueue) == -1){
		perror("ThpoolInit(): Could not allocate memory for job queue\n");
		free(thpool_p);
		return NULL;
	}

	/* Make threads in pool */
	thpool_p->threads = (struct thread**)malloc(num_threads * sizeof(struct thread *));
	if (thpool_p->threads == NULL){
		perror("ThpoolInit(): Could not allocate memory for threads\n");
		JobqueueDestroy(&thpool_p->jobqueue);
		free(thpool_p);
		return NULL;
	}

	pthread_mutex_init(&(thpool_p->thcount_lock), NULL);
	pthread_cond_init(&thpool_p->threads_all_idle, NULL);

	/* Thread init */
	int n;
	for (n=0; n<num_threads; n++){
		ThreadInit(thpool_p, &thpool_p->threads[n], n);
	}

	/* Wait for threads to initialize */
	while (thpool_p->num_threads_alive != num_threads) {}

	return thpool_p;
}

/************************************************************************************************
 * function name: ThpoolAddWork                                                                *
* The input: thpool_*, void (*function_p), void*                                                                   *
* The output: int                                                                                *
* The function operation: Add work to the threadpool                        *
*************************************************************************************************/
int ThpoolAddWork(thpool_* thpool_p, void (*function_p)(void*), void* arg_p) {
	job* newjob;

	newjob=(struct job*)malloc(sizeof(struct job));
	if (newjob==NULL){
		perror("ThpoolAddWork(): Could not allocate memory for new job\n");
		return -1;
	}

	/* add function and argument */
	newjob->function=function_p;
	newjob->arg=arg_p;

	/* add job to queue */
	JobqueuePush(&thpool_p->jobqueue, newjob);

	return 0;
}

/************************************************************************************************
 * function name: ThpoolWait                                                                *
* The input: thpool_*                                                                   *
* The output: void                                                                                *
* The function operation: Wait until all jobs have finished                        *
*************************************************************************************************/
void ThpoolWait(thpool_* thpool_p) {
	pthread_mutex_lock(&thpool_p->thcount_lock);
	int i;
	while (thpool_p->jobqueue.len || thpool_p->num_threads_working) {
		pthread_cond_wait(&thpool_p->threads_all_idle, &thpool_p->thcount_lock);
		ThreadIdentifier();
	}
	pthread_mutex_unlock(&thpool_p->thcount_lock);
}

/************************************************************************************************
 * function name: ThpoolPause                                                                *
* The input: thpool_*                                                                   *
* The output: void                                                                                *
* The function operation: Pause all threads in threadpool                       *
*************************************************************************************************/
void ThpoolPause(thpool_* thpool_p) {
	int n;
	for (n=0; n < thpool_p->num_threads_alive; n++){
		pthread_cancel(thpool_p->threads[n]->pthread);
	}
}

/************************************************************************************************
 * function name: ThreadInit                                                                *
* The input: thpool_*, thread**, int                                                      *
* The output: int                                                                                *
* The function operation: Initialize a thread in the threadpool                        *
*************************************************************************************************/
static int ThreadInit (thpool_* thpool_p, struct thread** thread_p, int id){

	*thread_p = (struct thread*)malloc(sizeof(struct thread));
	if (thread_p == NULL){
		perror("ThreadInit(): Could not allocate memory for thread\n");
		return -1;
	}

	(*thread_p)->thpool_p = thpool_p;
	(*thread_p)->id = id;

	pthread_create(&(*thread_p)->pthread, NULL, (void *)ThreadDo, (*thread_p));
	pthread_detach((*thread_p)->pthread);
	return 0;
}

/************************************************************************************************
 * function name: ThreadDo                                                                *
* The input: thread*                                                                   *
* The output: void *                                                                               *
* The function operation: run into an endless loop the threads                       *
*************************************************************************************************/
static void* ThreadDo(struct thread* thread_p){

	/* Set thread name for profiling and debuging */
	char thread_name[128] = {0};
	sprintf(thread_name, "thread-pool-%d", thread_p->id);

	prctl(PR_SET_NAME, thread_name);

	/* Assure all threads have been created before starting serving */
	thpool_* thpool_p = thread_p->thpool_p;

	/* Mark thread as alive (initialized) */
	pthread_mutex_lock(&thpool_p->thcount_lock);
	thpool_p->num_threads_alive += 1;
	pthread_mutex_unlock(&thpool_p->thcount_lock);

	while(threads_keepalive){

		sem_wait(thpool_p->jobqueue.has_jobs);

		if (threads_keepalive){

			pthread_mutex_lock(&thpool_p->thcount_lock);
			thpool_p->num_threads_working++;
			pthread_mutex_unlock(&thpool_p->thcount_lock);

			/* Read job from queue and execute it */
			void (*func_buff)(void*);
			void*  arg_buff;
			job* job_p = JobqueuePull(&thpool_p->jobqueue);
			if (job_p) {
				func_buff = job_p->function;
				arg_buff  = job_p->arg;
				func_buff(arg_buff);
				free(job_p);
			}

			pthread_mutex_lock(&thpool_p->thcount_lock);
			thpool_p->num_threads_working--;
			if (!thpool_p->num_threads_working) {
				pthread_cond_signal(&thpool_p->threads_all_idle);
			}
			pthread_mutex_unlock(&thpool_p->thcount_lock);

		}
	}
	pthread_mutex_lock(&thpool_p->thcount_lock);
	thpool_p->num_threads_alive --;
	pthread_mutex_unlock(&thpool_p->thcount_lock);

	return NULL;
}

/************************************************************************************************
 * function name: JobqueueInit                                                                *
* The input: jobqueue*                                                                   *
* The output: int                                                                                *
* The function operation: Initialize queue                        *
*************************************************************************************************/
static int JobqueueInit(jobqueue* jobqueue_p){
	jobqueue_p->len = 0;
	jobqueue_p->front = NULL;
	jobqueue_p->rear  = NULL;

	jobqueue_p->has_jobs = (sem_t*)malloc(sizeof(sem_t));
	if (jobqueue_p->has_jobs == NULL){
		return -1;
	}

	pthread_mutex_init(&(jobqueue_p->rwmutex), NULL);
	sem_init(jobqueue_p->has_jobs, 0, 1);

	return 0;
}

/************************************************************************************************
 * function name: JobqueueClear                                                                *
* The input: jobqueue*                                                                   *
* The output: void                                                                                *
* The function operation: Clear the queue                        *
*************************************************************************************************/
static void JobqueueClear(jobqueue* jobqueue_p){

	while(jobqueue_p->len){
		free(JobqueuePull(jobqueue_p));
	}

	jobqueue_p->front = NULL;
	jobqueue_p->rear  = NULL;
	sem_init(jobqueue_p->has_jobs, 0, 1);
	jobqueue_p->len = 0;

}

/************************************************************************************************
 * function name: JobqueuePush                                                                *
* The input: jobqueue*, job*                                                                   *
* The output: void                                                                                *
* The function operation: Add (allocated) job to queue                        *
*************************************************************************************************/
static void JobqueuePush(jobqueue* jobqueue_p, struct job* newjob){

	pthread_mutex_lock(&jobqueue_p->rwmutex);
	newjob->prev = NULL;

	switch(jobqueue_p->len){

		case 0:  /* if no jobs in queue */
					jobqueue_p->front = newjob;
					jobqueue_p->rear  = newjob;
					break;

		default: /* if jobs in queue */
					jobqueue_p->rear->prev = newjob;
					jobqueue_p->rear = newjob;

	}
	jobqueue_p->len++;

	sem_post(jobqueue_p->has_jobs);
	pthread_mutex_unlock(&jobqueue_p->rwmutex);
}

/************************************************************************************************
 * function name: JobqueuePull                                                                *
* The input: jobqueue*                                                                   *
* The output: job*                                                                                *
* The function operation: Get first job from queue(removes it from queue)                        *
*************************************************************************************************/
static struct job* JobqueuePull(jobqueue* jobqueue_p){

	pthread_mutex_lock(&jobqueue_p->rwmutex);
	job* job_p = jobqueue_p->front;

	switch(jobqueue_p->len){

		case 0:  /* if no jobs in queue */
		  			break;

		case 1:  /* if one job in queue */
					jobqueue_p->front = NULL;
					jobqueue_p->rear  = NULL;
					jobqueue_p->len = 0;
					break;

		default: /* if >1 jobs in queue */
					jobqueue_p->front = job_p->prev;
					jobqueue_p->len--;
					/* more than one job in queue -> post it */
					sem_post(jobqueue_p->has_jobs);

	}

	pthread_mutex_unlock(&jobqueue_p->rwmutex);
	return job_p;
}

/************************************************************************************************
 * function name: JobqueueDestroy                                                                *
* The input: jobqueue*                                                                   *
* The output: void                                                                                *
* The function operation: Free all queue resources back to the system                        *
*************************************************************************************************/
static void JobqueueDestroy(jobqueue* jobqueue_p){
	JobqueueClear(jobqueue_p);
	free(jobqueue_p->has_jobs);
}

/************************************************************************************************
 * function name: SleepAndAdd                                                                *
* The input: void*                                                                   *
* The output: void                                                                                *
* The function operation: sleep a randomally number of nano seconds and add a number to internal_count *
***************************************************************************************************/
void SleepAndAdd(void* number_to_add) {
	int x = rand() % 91 + 10;
	struct timespec tim, start;
	tim.tv_sec = 0;
	tim.tv_nsec = x;
	start.tv_sec = 0;
	start.tv_nsec = 0;
	int number = (intptr_t) number_to_add;
	nanosleep(&start, &tim);
	internal_count = internal_count + number;
}

/************************************************************************************************
 * function name: ThreadIdentifier                                                                *
* The input:                                                                    *
* The output: void                                                                                *
* The function operation: write into the file the thread identifier of the current thread and the  *
* current internal_count                                                *
***************************************************************************************************/
void ThreadIdentifier() {
	int length = snprintf(NULL, 0, "%lu", pthread_self());
	char pthread_string[length+1];
	snprintf(pthread_string, length+1, "%lu", pthread_self());
	pthread_string[length] = '\0';
	PrintString("thread identifier is ");
	PrintString(pthread_string);
	PrintString(" and internal_count is ");

	int lengthC = snprintf(NULL, 0, "%d", internal_count);
	char count_string[lengthC+1];
	snprintf(count_string, lengthC+1, "%d", internal_count);
	count_string[length] = '\0';
	PrintString(count_string);
	PrintString("\n");
}

/************************************************************************************************
 * function name: PrintString                                                                *
* The input: string                                                                   *
* The output: void                                                                                *
* The function operation: write a string on the file                                                *
***************************************************************************************************/
void PrintString(char* stringToPrint) {
	if(write(fd, stringToPrint, strlen(stringToPrint)) != strlen(stringToPrint)) {
		perror("write error");
		exit(-1);
	}
}

/*************************************************************************************************
* function name: main                                                                            *
* The input:                                                                    *
* The output:                                                                                 *
* The function operation:   *
**************************************************************************************************/
int main(int argc, char *argv[]) {
	int x = 0;
	int i;
	fd = open("336141056.txt", O_RDWR | O_CREAT, S_IRUSR | S_IWUSR, 00666);
	char userControl;
	internal_count = 0;
	if(fd < 0) {
		perror("336141056.txt open");
		exit(-1);
	}
	char* sharestr = malloc(1);
	if(sharestr == NULL) {
		perror("malloc");
		exit(-1);
	}
	srand((unsigned int) time(0));
	int shmid;
	key_t key;

	/*
	 * Creation of the shared memory
	 */

	/* make the key: */
	if ((key = ftok("336141056.txt", 'A')) == -1) {
		perror("ftok");
		exit(-1);
	}

	/* connect to (and possibly create) the segment: */
	if((shmid = shmget(key, SHM_SIZE, 0644 | IPC_CREAT)) == -1) {
		perror("shmid");
		exit(-1);
	}

	/* attach to the segment to get a pointer to it: */
	sharestr = shmat(shmid, NULL, 0);
	if (sharestr == (char *)(-1)) {
		perror("shmat");
		exit(1);
	}
	sem_read=sem_open("/semwriting", O_CREAT, S_IRUSR | S_IWUSR, 0);
	sem_write=sem_open("/semreading", O_CREAT, S_IRUSR | S_IWUSR, 1);
	if (sem_read == SEM_FAILED) {
		perror("sem_open read");
		exit(-1);
	}
	if (sem_write == SEM_FAILED) {
		perror("sem_open write");
		exit(-1);
	}
	/* Creation of the threadpool */
	thpool_* threadpool = ThpoolInit(5);

	userControl = ' ';
	while(userControl != 'g' && userControl != 'h') {
		if(sem_wait(sem_read) < 0) {
			perror("sem_wait()");
			exit(-1);
		}
		userControl = sharestr[0];
		switch(userControl) {
			case 'a':
				ThpoolAddWork(threadpool, SleepAndAdd, (void*) (intptr_t) 1);
				break;
			case 'b':
				ThpoolAddWork(threadpool, SleepAndAdd, (void*) (intptr_t) 2);
				break;
			case 'c':
				ThpoolAddWork(threadpool, SleepAndAdd, (void*) (intptr_t) 3);
				break;
			case 'd':
				ThpoolAddWork(threadpool, SleepAndAdd, (void*) (intptr_t) 4);
				break;
			case 'e':
				ThpoolAddWork(threadpool, SleepAndAdd, (void*) (intptr_t) 5);
				break;
			case 'f':
				ThpoolAddWork(threadpool, ThreadIdentifier, NULL);
				break;
			case 'g':
				ThpoolPause(threadpool);
				ThreadIdentifier();
				break;
			case 'h':
				ThpoolWait(threadpool);
				ThreadIdentifier();
				break;
		}
	}

	if(sem_unlink("/semwriting") < 0) {
		perror("sem_unlink");
	}
	if(sem_unlink("/semreading") < 0) {
		perror("sem_unlink");
	}
	/* detach from the segment: */
	if((shmctl (shmid, IPC_RMID, (struct shmid_ds *) 0)) == -1) {
		perror("shmdt");
		exit(1);
	}
	exit(0);
}
