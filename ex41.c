/****************************************
* Student name: Adele Bendayan
* Student ID: 336141056
* Course Exercise Group: 05
* Exercise name: Exercise 04 part 1
*****************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/stat.h>
#include <sys/shm.h>
#include <fcntl.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <stdbool.h>
#include <signal.h>
#include <string.h>
#include <ctype.h>
#include <semaphore.h>

#define SHM_SIZE 1024
sem_t * sem_read;
sem_t * sem_write;

/*************************************************************************************************
* function name: main                                                                            *
* The input:                                                                    *
* The output:                                                                                 *
* The function operation: connect to the shared memory, ask for user input, send it to the server *
**************************************************************************************************/
int main(int argc, char *argv[]) {

	char *memory = malloc(2); // there is only one message in the shared memory
	int x, y;
	pid_t pid;
	char userOperation = ' ';
	int shmid;
	key_t key;
	if(memory == NULL) {
		perror("malloc");
		exit(-1);
	}
	if ((key = ftok("336141056.txt", 'A')) == -1) {
		perror("ftok");
		exit(1);
	}

	/* grab the shared memory created by ex42.c: */
	if ((shmid = shmget(key, SHM_SIZE, 0)) == -1) {
		perror("shmget");
		exit(1);
	}

	/*
	 * Now we attach the segment to our data space.
	 */
	if ((memory = shmat(shmid, NULL, 0)) == (char *) -1) {
		perror("shmat");
		exit(1);
	}
	sem_read=sem_open("/semwriting", O_CREAT, S_IRUSR | S_IWUSR, 0);
	sem_write=sem_open("/semreading", O_CREAT, S_IRUSR | S_IWUSR, 1);

	int value;
	printf("Please enter request code\n");
	scanf("%c", &userOperation);
	while(userOperation != 'i') {
		// lock the "write" semaphore
		if(sem_wait(sem_write) < 0) {
			perror("sem_wait()");
			exit(-1);
		}
		userOperation = tolower(userOperation);
		memory[0] = userOperation;
		memory[1] = '\0';
		// tell the other that he finished writing
		if(sem_post(sem_write) < 0) {
			perror("sem_post()");
		}
		// tell the server (ex42) that there is news in the shared memory
		if(sem_post(sem_read) < 0) {
			perror("sem_post()");
		}
		printf("Please enter request code\n");
		scanf(" %c", &userOperation);
	}
	exit(0);
}
